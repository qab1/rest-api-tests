package product;

import com.google.inject.Guice;
import com.google.inject.Injector;
import dto.CategoriesItem;
import dto.ProductStructure;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import module.ApiClientModule;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;
import services.ProductApi;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.hamcrest.Matchers.equalTo;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductApiTest {

    private Injector injector = Guice.createInjector(new ApiClientModule());
    private ProductApi productApi = injector.getInstance(ProductApi.class);

    private static final Integer amount = 12;
    private static final Integer discount = 20;
    private static final String name = "Jeans";
    private static final String brand = "Ostin";
    private static final Boolean isVisible = true;
    private static String id;

    @Test
    @Order(1)
    @DisplayName("Создание товара через апи")
    public void createProductTest() {
        //1 Создать и заполнить структуру товара
        //2 Создать товар через POST запрос
        //3 Проверить статус код запроса - 200
        //4 Проверить что amount ответа совпадает с amount запроса
        //5 Проверить что name ответа совпадает с name запроса
        //6 Проверить что brand ответа совпадает с brand запроса
        //7 Проверить что discount ответа совпадает с discount запроса

        Response response;

        ProductStructure productStructure = new ProductStructure();
        CategoriesItem categoriesItem = new CategoriesItem(name, isVisible, brand);
        productStructure.setAmount(amount);
        productStructure.setCategories(asList(categoriesItem));
        productStructure.setDiscount(discount);
        productStructure.setName(name);
        productStructure.setVisible(isVisible);

        response = productApi.createProduct(productStructure);

        JsonPath jsonPath = response.jsonPath();
        id = jsonPath.getString("values[0].id");
        System.out.println("-----------------------");
        System.out.println(id);

        response
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_CREATED)
                .body("values[0].productName", equalTo(name))
                .body("values[0].amount", equalTo(amount))
                .body("values[0].discount", equalTo(discount))
                .body("values[0].categories[0].brand", equalTo(brand));
    }

    @Test
    @Order(2)
    @DisplayName("Получение товара по id")
    public void getProductTest() {
        //1 Запросить товар через GET запрос передав id
        //2 Проверить статус код ответа - 200
        //3 Проверить что amount ответа совпадает с amount запроса
        //4 Проверить что name ответа совпадает с name запроса
        //5 Проверить что brand ответа совпадает с brand запроса
        //6 Проверить что discount ответа совпадает с discount запроса

        Response response;
        response = productApi.getProduct(id);

        response
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("value.productName", equalTo(name))
                .body("value.amount", equalTo(amount.floatValue()))
                .body("value.discount", equalTo(discount.floatValue()))
                .body("value.categories[0].brand", equalTo(brand));
    }

    @Test
    @Order(3)
    @DisplayName("Удаление товара по id")
    public void deleteProductTest() {
        //1 Удалить товар через DELETE запрос передав id
        //2 Проверить статус код ответа - 200
        //3 Проверить что amount ответа совпадает с amount запроса
        //4 Проверить что name ответа совпадает с name запроса
        //5 Проверить что brand ответа совпадает с brand запроса
        //6 Проверить что discount ответа совпадает с discount запроса

        Response response;
        response = productApi.deleteProduct(id);

        response
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("value.productName", equalTo(name))
                .body("value.amount", equalTo(amount.floatValue()))
                .body("value.discount", equalTo(discount.floatValue()))
                .body("value.categories[0].brand", equalTo(brand));
    }
}
