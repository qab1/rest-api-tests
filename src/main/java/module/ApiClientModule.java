package module;

import com.google.inject.AbstractModule;
import services.ProductApi;

public class ApiClientModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ProductApi.class).asEagerSingleton();
    }
}
