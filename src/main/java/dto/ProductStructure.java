package dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
public class ProductStructure {
	private int amount;
	private String name;
	private int discount;
	private List<CategoriesItem> categories;
	private boolean isVisible;
	private int percentDiscount;
}