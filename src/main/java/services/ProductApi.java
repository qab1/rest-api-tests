package services;

import dto.ProductStructure;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class ProductApi {
    private RequestSpecification spec;
    private static final String PRODUCT = "/product";

    public ProductApi() {
        spec = given()
                .baseUri(System.getProperty("api.base.url"))
                .contentType(ContentType.JSON);
    }

    public Response createProduct(ProductStructure product) {
        return given(spec)
                .with()
                .body(product)
                .log().all()
                .when()
                .post(PRODUCT);
    }

    public Response getProduct(String id) {
        return given(spec)
                .with()
                .log().all()
                .when()
                .get(PRODUCT + String.format("/%s", id));
    }

    public Response deleteProduct(String id) {
        return given(spec)
                .with()
                .log().all()
                .when()
                .delete(PRODUCT + String.format("/%s", id));
    }
}
